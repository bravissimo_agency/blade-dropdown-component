import vue from 'rollup-plugin-vue'

export default {
  input: 'src/js/index.js',
  output: {
    format: 'esm',
    file: 'dist/DropdownProvider.js'
  },
  plugins: [
    vue()
  ]
}