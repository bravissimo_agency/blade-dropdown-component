var script = {
    model: {
        prop: 'active',
        event: 'change'
    },

    props: {
        id: {
            type: String,
            required: true
        },

        items: {
            type: Array,
            required: true
        },

        active: {
            type: [Number, String, Boolean],
            default: null
        },

        excludeActive: {
            type: Boolean,
            default: false
        }
    },

    data: () => ({
        isOpen: false
    }),

    computed: {
        activeItem () {
            if (!this.active) {
                return this.items[0];
            }

            return this.items.find(item => item.value === this.active) || this.items[0];
        },

        selectableItems () {
            if (!this.excludeActive) {
                return this.items;
            }

            return this.items.filter(item => item.value !== this.active);
        }
    },

    methods: {
        toggle () {
            this.isOpen = !this.isOpen;
        },

        close () {
            this.isOpen = false;
        },

        selectItem (item) {
            this.$emit('change', item.value);
            this.close();
        }
    },

    render () {
        return this.$scopedSlots.default({
            $toggle: this.toggle,
            $close: this.close,
            $selectItem: this.selectItem,
            id: this.id,
            isOpen: this.isOpen,
            items: this.selectableItems,
            activeItem: this.activeItem
        });
    }
};

var script$1 = {
    props: {
        activeItem: {
            type: Object,
            default: null
        },

        noSelectedLabel: {
            type: String,
            default: null
        }
    },

    computed: {
        item () {
            if ((!this.activeItem || !this.activeItem.value) && this.noSelectedLabel) {
                return {
                    label: this.noSelectedLabel,
                    value: null
                }
            }

            return this.activeItem;
        }
    },

    render () {
        return this.$scopedSlots.default(this.item);
    }
};

const Components = {
    DropdownProvider: script,
    DropdownButtonProvider: script$1,
};

var index = {
    install (Vue) {
        Object.keys(Components).forEach(name => {
            Vue.component(name, Components[name]);
        });
    }
};

export default index;
