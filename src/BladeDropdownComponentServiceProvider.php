<?php

namespace Bravissimo\BladeDropdownComponent;

use Illuminate\Support\ServiceProvider;

class BladeDropdownComponentServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadViewsFrom(__DIR__, 'dropdown');
    }
}
