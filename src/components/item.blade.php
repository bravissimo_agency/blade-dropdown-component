<li
    v-for='item in DropdownProvider.items'
    v-bind:key="item.value"
    v-bind:aria-selected="item === DropdownProvider.activeItem"
    v-bind:class="{ 'isActive': item === DropdownProvider.activeItem }"
    role="options"
    @click="DropdownProvider.$selectItem(item)"
    {{ $attributes->merge(['class' => 'dropdownItem']) }}
>
    @if (!empty($slot) && !empty($slot->toHtml()))
        {!! $slot !!}
    @else 
        @{{ item.label }}
    @endif
</li>