@props([
    'id' => base64_encode(random_bytes(10)),
    'excludeActive' => false,
    'items',
])

<dropdown-provider
    id="{{ $id }}"
    v-slot:default="DropdownProvider"
    :items='@json($items)'
    :exclude-active="@json($excludeActive)"
    {{ $attributes->only('v-model') }}
>
    <div
        v-esc="DropdownProvider.$close"
        v-click-outside="DropdownProvider.$close"
        :class="{ 'isOpen': DropdownProvider.isOpen }"
        {{ $attributes->except('v-model')->merge(['class' => 'dropdown']) }}
    >
        {!! $slot !!}
    </div>
</dropdown-provider>