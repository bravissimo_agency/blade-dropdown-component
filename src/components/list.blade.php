<transition name="dropdownList">
    <ul
        v-show="DropdownProvider.isOpen"
        v-bind:id="DropdownProvider.id + '__list'"
        v-bind:aria-labelledby="DropdownProvider.id + '__button'"
        role="listbox"
        v-cloak
        {{ $attributes->merge(['class' => 'dropdownList customScrollbar']) }}
    >
        {!! $slot !!}
    </ul>
</transition>