import DropdownProvider from './DropdownProvider.vue';
import DropdownButtonProvider from './DropdownButtonProvider.vue';

const Components = {
    DropdownProvider,
    DropdownButtonProvider,
};

export default  {
    install (Vue) {
        Object.keys(Components).forEach(name => {
            Vue.component(name, Components[name]);
        });
    }
};
